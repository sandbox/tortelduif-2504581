<?php

/**
 * Implements hook_rules_action_info().
 */
function mailcomment_rules_rules_action_info() {
  return array(
    'mail_comment_rules' => array(
      'label' => t('Send mail with mailcomment support'),
      'group' => t('System'),
      'parameter' => array(
        'to' => array(
          'type' => 'text',
          'label' => t('To'),
          'description' => t('The e-mail address or addresses where the message will be sent to. The formatting of this string must comply with RFC 2822.'),
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject'),
          'description' => t("The mail's subject."),
          'translatable' => TRUE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Message'),
          'description' => t("The mail's message body."),
          'translatable' => TRUE,
        ),
        'from' => array(
          'type' => 'text',
          'label' => t('From'),
          'description' => t("The mail's from address. Leave it empty to use the site-wide configured address."),
          'optional' => TRUE,
        ),
        'node' => array(
          'type' => 'node',
          'label' => t('Node'),
          'description' => t('The node to generate the id'),
          'optional' => FALSE,
        ),
        'comment' => array(
          'type' => 'comment',
          'label' => t('comment'),
          'description' => t('The comment to generate the id'),
          'optional' => TRUE,
        ),
        'language' => array(
          'type' => 'token',
          'label' => t('Language'),
          'description' => t('If specified, the language used for getting the mail message and subject.'),
          'options list' => 'entity_metadata_language_list',
          'optional' => TRUE,
          'default value' => LANGUAGE_NONE,
          'default mode' => 'selector',
        ),
      ),
      'base' => 'mailcomment_rules_action_mail',
      'access callback' => 'rules_system_integration_access',
    ),
  );
}

/**
 * Action Implementation: Send mail.
 */
function mailcomment_rules_action_mail($to, $subject, $message, $from = NULL, $node, $comment=NULL, $langcode, $settings, RulesState $state, RulesPlugin $element) {
  $to = str_replace(array("\r", "\n"), '', $to);
  $from = !empty($from) ? str_replace(array("\r", "\n"), '', $from) : NULL;
  $params = array(
    'subject' => $subject,
    'message' => $message,
    'langcode' => $langcode,
  );

  // Set a unique key for this mail.
  $name = isset($element->root()->name) ? $element->root()->name : 'unnamed';
  $key = 'rules_action_mail_' . $name . '_' . $element->elementId();
  $languages = language_list();
  $language = isset($languages[$langcode]) ? $languages[$langcode] : language_default();

  // Generate our comment id.
  $generatorarray = array(
    'uid' => $node->uid,
    'nid' => $node->nid,
    'cid' => isset($comment->cid) ? $comment->cid : 0,
    'time' => $node->changed,
  );
  $messagecommentid = mailcomment_build_messageid($generatorarray);
  // Add the key to our params.
  $params['messagecommentid'] = $messagecommentid;

  $message = drupal_mail('mailcomment_rules', $key, $to, $language, $params, $from);
  if ($message['result']) {
    watchdog('rules', 'Successfully sent email to %recipient', array('%recipient' => $to));
  }
}

/**
 * Implements hook_mail().
 */
function mailcomment_rules_mail($key, &$message, $params) {
  $message['body'] = array(nl2br($message['params']['message']));
  $message['subject'] = $message['params']['subject'];
  $message['headers']['Message-ID'] = $message['params']['messagecommentid'];
  $message['headers']['Content-Type'] = "text/html; charset=\"UTF-8\"";
}
