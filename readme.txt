CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * configuration

INTRODUCTION
------------
The mailcomment_rules module, allows you to use mailcomment module without
the use of message notify or notifications.

By using rules it allows alot more flexibility, less configuration and more
template options.

REQUIREMENTS
------------
This module depends on Comment, Mailcomment, Rules and Mailhandler.

RECOMMENDED MODULES
-------------------
I recommend you use smtp for outgoing e-mails.

INSTALLATION
------------
Install like any other drupal module. The mailcomments_rules module does
not include a configuration menu.

CONFIGURATION
-------------
Enable the module, then configure your rules to use the "Send mail with
mailcomment support"
Assign the node and if available the comment.
Configure your subject, recipient and body. The module will take care of all
the rest.
